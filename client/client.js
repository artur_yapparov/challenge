document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('start').addEventListener('click', () => {
    const numberOfTeams = parseInt(document.getElementById('numberOfTeams').value);

    if (!numberOfTeams) return false;

    const tournament = new TournamentClient(numberOfTeams);
    tournament.startTournament();
  });
});
