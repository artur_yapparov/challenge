const API = {
  tournament: {
    path: '/tournament',
  },
  team: {
    path: '/team',
    method: 'GET',
  },
  match: {
    path: '/match',
    method: 'GET',
  },
  winner: {
    path: '/winner',
    method: 'GET',
  },
};

class Request {

  static sendRequest(method, path, data) {
    if (method && method.toUpperCase() === 'GET') {
      return this.getRequest(path, data);
    }
    return this.postRequest(path, data);
  }

  static postRequest(path, data) {
    const searchParams = Object.keys(data).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');

    return fetch(path, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: searchParams,
    }).then(result => result.json());
  }

  static getRequest(path, data) {
    const url = new URL(`${location.origin}${path}`);
    Object.keys(data).forEach(key => {
      if (data[key].constructor === Array) {
        return data[key].forEach((item) => {
          url.searchParams.append(key, item);
        });
      }
      return url.searchParams.append(key, data[key])
    });
    return fetch(url).then(result => result.json());
  }

  static getWinnerInfo(tournamentId, teamScores, matchScore) {
    return this.sendRequest(API.winner.method, API.winner.path, {
      tournamentId,
      teamScores,
      matchScore,
    });
  }

  static getTeamInfo(tournamentId, teamId) {
    return this.sendRequest(API.team.method, API.team.path, {
      tournamentId,
      teamId,
    });
  }

  static getMatchInfo(tournamentId, round, match) {
    return this.sendRequest(API.match.method, API.match.path, {
      tournamentId,
      round,
      match,
    });
  }

  static createTournament(numberOfTeams) {
    return this.sendRequest(API.tournament.method, API.tournament.path, {
      numberOfTeams,
    });
  }

}
