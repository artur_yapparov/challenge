class TournamentOutputController {
  constructor(containerId) {
    this.textContainer = document.getElementById(containerId);
    if (document.getElementById('squaresContainer')) {
      this.squaresContainer = document.getElementById('squaresContainer');
      this.squaresContainer.innerHTML = '';
    } else {
      this.squaresContainer = document.createElement('div');
      this.squaresContainer.setAttribute('id','squaresContainer');
      this.textContainer.parentNode.insertBefore(this.squaresContainer, this.textContainer.nextSibling);
    }
  }

  addSquares(numberOfSquares) {
    Array(numberOfSquares).fill(null).forEach(this.appendSquare.bind(this));
  }

  appendSquare() {
    const div = document.createElement('div');
    div.className = 'square';
    this.squaresContainer.appendChild(div);
  }

  clearText() {
    this.textContainer.innerHTML = '';
  }

  printText(text) {
    this.textContainer.innerHTML = text;
  }

  fillSquare() {
    const square = this.squaresContainer.querySelector('.square:not(.filled)');
    if (square) square.className = 'square filled';
  }

}
