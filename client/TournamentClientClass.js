const ELEMENTS_IDS = {
  numberOfTeams: 'numberOfTeams',
  winner: 'winner',
};

class TournamentClient {

  constructor(numberOfTeams) {
    this.numberOfTeams = numberOfTeams;
    this.teamsDataByScore = {};
    this.matches = {};
    this.output = new TournamentOutputController(ELEMENTS_IDS.winner, this.numberOfTeams);
  }

  startTournament() {
    Request.createTournament(this.numberOfTeams)
      .then((result) => {
        if (result.error) {
          this.output.printText(result.message);
          return;
        }

        const allTeamsInTournament = result.matchUps.reduce(
          (previousValue, currentItem) => [...previousValue, ...currentItem.teamIds],
          []
        );

        this.output.clearText();
        this.output.addSquares(this.getNumberOfMatches(this.numberOfTeams));
        this.tournamentId = result.tournamentId;
        this.getMatchesOfTournament(this.numberOfTeams);
        this.getTeamsOfTournament(allTeamsInTournament);
      });
  }

  getTeamsOfTournament(teams) {
    teams.forEach(this.getTeamRequest.bind(this));
  }

  getTeamRequest(team, index) {
    Request.getTeamInfo(this.tournamentId, team)
      .then((result) => {
        const gameNumber = parseInt(index / TEAMS_PER_MATCH);
        this.matches[0][gameNumber].teamIds.push(result.score);
        this.teamsDataByScore[result.score] = result;
        this.playMatch(0, gameNumber);
      });
  }

  getMatchesOfTournament(numberOfTeams) {
    let teams_left = numberOfTeams;
    let round = 0;

    if (!(parseInt(teams_left) > 1 && parseInt(TEAMS_PER_MATCH) > 1)) {
      return false;
    }
    while (teams_left > 1) {
      teams_left = teams_left / TEAMS_PER_MATCH;
      this.getMatchesOfRound(teams_left, round);
      round++;
    }
  }

  getMatchesOfRound(teams_left, round) {
    Array(teams_left).fill(null).forEach((item, index) => {
      Request.getMatchInfo(this.tournamentId, round, index)
        .then((result) => {
          this.matches[round][index].matchScore = result.score;
          this.playMatch(round, index);
        });
    });
  }

  playMatch(round, match) {
    const matchScore = this.matches[round][match].matchScore;
    const teamIds = this.matches[round][match].teamIds;

    if (matchScore === undefined || teamIds.length !== TEAMS_PER_MATCH) {
      return false;
    }

    Request.getWinnerInfo(this.tournamentId, teamIds, matchScore)
      .then((result) => {
        this.output.fillSquare();
        if (this.matches[round + 1]) {
          this.matches[round + 1][parseInt(match / TEAMS_PER_MATCH)].teamIds.push(result.score);
          this.playMatch(round + 1, parseInt(match / TEAMS_PER_MATCH));
        } else {
          const winnerText = `${this.teamsDataByScore[result.score].name}`;
          this.output.printText(winnerText);
        }
      });
  }

  getNumberOfMatches(numberOfTeams) {
    let numberOfMatches = 0;
    let round = 0;

    if (!(parseInt(numberOfTeams) > 0) && parseInt(numberOfTeams) >= parseInt(TEAMS_PER_MATCH)) {
      return 0;
    }
    while (numberOfTeams % TEAMS_PER_MATCH === 0) {
      numberOfTeams /= TEAMS_PER_MATCH;
      numberOfMatches += numberOfTeams;
      for (let i = 0; i < numberOfTeams; ++i) {
        if (!this.matches[round]) this.matches[round] = {};
        this.matches[round][i] = {
          teamIds: [],
        }
      }
      ++round;
    }

    return numberOfMatches;
  }

}
